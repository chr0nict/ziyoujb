<center>
  <a href=""><img src="https://cdn.maxbridgland.com/ziyou_banner.png"></a>
  <br>
  <h1 align="center">Ziyou Jailbreak</h1>
  <h5 align="center">Bringing Freedom To Your Device</h5>
</center>

# What Is This?

This is the official public mirror for the Ziyou Git Repository. This will be periodically updated upon release of new versions. **In the current state this is not a complete product. We are using this as a timestamp to show when we had our code out there for future reference.** If you are interested in contributing to this project please checkout the Contributing section of this README. The Ziyou Jailbreak is a WIP and we are not responsible for any damage that occurs on your device due to use of this software. This software is licensed under the GNU GPLv3 License.

This software was built from research from a bunch of different developers. The final product is the work of myself and the Slice team. We decided to work together with the research we both had and build this. Once we release the product all credit for everything used will be given down below. For now it's the main contributors.

# Credit

@Jakeashacks (For helping with bug fixes, jelbreklib, and more)

@Pwn20wnd  (For helping @Chr0nicT understand some stuff, remount, sandbox extensions function for jailbreakd, exploits)

@iBSparkes (Exploits)

@_bazad (Exploits, kernel_call)

@xerub (patchfinder64)

@comex (Substitute)

@saurik (Cydia)

@maxbridgland

@Chr0nicT

@BrandonPlank6

@ConorTheDev

@DaveWijk

