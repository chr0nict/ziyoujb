//
//  runSockPuppet.h
//  Ziyou
//
//  Created by Tanay Findley on 7/11/19.
//  Copyright © 2019 Ziyou Team. All rights reserved.
//

#ifndef runSockPuppet_h
#define runSockPuppet_h

#include <stdio.h>

void runSockPuppet(void);

#endif /* runSockPuppet_h */
